#pragma once
#include "irq.h"

void serialSetup(int speed);
void serialPutc(char a);
void serialDoIrq(struct cpu_state *state);
