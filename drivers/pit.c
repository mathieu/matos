#include "pit.h"
#include "io.h"
#include "thread.h"
#include "time.h"

int pitSetup(unsigned int freq)
{
    unsigned int divisor = PIT_FREQ / freq;
    if (divisor > 65535)
        divisor = 0;     // Used to represent 35536
    outb(PIT_CMD, 0x34); // chan 0; low then high; mode 2
    outb(PIT_CHAN_0, divisor & 0xFF);
    outb(PIT_CHAN_0, divisor >> 8u);

    return 0;
}

struct cpu_state *pitIrqHandler(struct cpu_state *prevCpu)
{
    __atomic_add_fetch(&jiffies, 1, __ATOMIC_RELAXED);
    threadOnJieffiesTick();
    // Uncomment for non-preemptible kernel
    if (cpu_context_is_in_user_mode(prevCpu)) {
        return threadSwitch(prevCpu);
    } else {
        return prevCpu;
    }
}
