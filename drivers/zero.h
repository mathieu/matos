#pragma once


#include "types.h"
#include "uaddrspace.h"

#include <stddef.h>
#include <stdint.h>
int zeroSetup();

int zeroMmap(struct uAddrSpace *as, uaddr_t *uaddr, size_t size, uint32_t rights, uint32_t flags);
