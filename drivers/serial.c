#include "serial.h"
#include "io.h"
#include "irq.h"

#define PORT 0x3f8 /* COM1 */
#define SERIAL_MAX_SPEED 115200

/* The type of parity.  */
#define UART_NO_PARITY 0x00
#define UART_ODD_PARITY 0x08
#define UART_EVEN_PARITY 0x18

/* The type of word length.  */
#define UART_5BITS_WORD 0x00
#define UART_6BITS_WORD 0x01
#define UART_7BITS_WORD 0x02
#define UART_8BITS_WORD 0x03

/* The type of the length of stop bit.  */
#define UART_1_STOP_BIT 0x00
#define UART_2_STOP_BITS 0x04

void serialSetup(int speed)
{
    unsigned short div = SERIAL_MAX_SPEED / speed;
    outb(PORT + 1, 0x00);       // Disable all interrupts
    outb(PORT + 3, 0x80);       // Enable DLAB (set baud rate divisor)
    outb(PORT + 0, div & 0xFF); // Set divisor lo byte
    outb(PORT + 1, div >> 8);   // Set divisor hi byte
    outb(PORT + 3,
         UART_NO_PARITY | UART_8BITS_WORD |
             UART_1_STOP_BIT); // 8 bits, no parity, one stop bit
    outb(PORT + 2, 0xC7);      // Enable FIFO, clear them, with 14-byte threshold
    outb(PORT + 4, 0x0B);      // IRQs enabled, RTS/DSR set
    irqSetRoutineWrapped(IRQ_COM1, serialDoIrq);
}

int isTransmitEmpty()
{
    return (inb(PORT + 5) & 0x20);
}

void serialPutc(char a)
{
    while (isTransmitEmpty() == 0)
        ;

    outb(PORT, a);
}

void serialDoIrq(struct cpu_state *state)
{
    (void)state;
    char c = inb(PORT);
    serialPutc(c);
}
