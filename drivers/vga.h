#pragma once
#include "stdint.h"
#include "types.h"

// https://wiki.osdev.org/Text_UI
#define BLACK 0x00
#define BLUE 0x01
#define GREEN 0x02
#define CYAN 0x03
#define RED 0x04
#define MAGENTA 0x05
#define BROWN 0x06
#define GREY 0x07
#define WHITE 0x0F

#define VGA_ADDR 0xB8000
#define VGA_WIDTH 80
#define VGA_HEIGHT 25

#define VGA_STATUS_LINE_HEIGHT 1

int VGASetup(uint bgColor, uint color);
void VGAPutc(const char str);
int VGAConfigure(int addr, uint width, uint height);
void VGAPrintf(uint color, uint bgColor, int startX, int startY, const char *format, ...);
void VGAClearLine(uint bgColor, uint line);
void VGAScrollUp(void);
void VGAMap(void);
