#pragma once
#include "stdarg.h"
#include "synchro.h"

#define ATA_PIO_DATA 0
#define ATA_PIO_ERR 1
#define ATA_PIO_FEAT 1
#define ATA_PIO_SEC_COUNT 2
#define ATA_PIO_LBALO 3
#define ATA_PIO_LBAMID 4
#define ATA_PIO_CYL_LOW 4
#define ATA_PIO_LBAHI 5
#define ATA_PIO_CYL_HI 5
#define ATA_PIO_DRIVE 6
#define ATA_PIO_STATUS 7
#define ATA_PIO_CMD 7

// cmd details
// https://people.freebsd.org/~imp/asiabsdcon2015/works/d2161r5-ATAATAPI_Command_Set_-_3.pdf
#define ATA_PIO_CMD_IDENTIFY 0xEC       /* get ATA params */
#define ATA_PIO_CMD_ATAPI_IDENTIFY 0xA1 /* get ATAPI params*/
#define ATA_PIO_CMD_READ 0x20           /* read command */
#define ATA_PIO_CMD_WRITE 0x30          /* write command */
#define ATA_PIO_CMD_READ_MULTI 0xC4     /* read multi command */
#define ATA_PIO_CMD_WRITE_MULTI 0xC5    /* write multi command */
#define ATA_PIO_CMD_SET_MULTI 0xC6      /* set multi size command */
#define ATA_PIO_CMD_PACKET_CMD 0xA0     /* set multi size command */

#define ATA_PIO_DRIVE_IBM 0xA0    /* bits that must be set */
#define ATA_PIO_DRIVE_LBA 0x40    /* use LBA ? */
#define ATA_PIO_DRIVE_MASTER 0x00 /* select master */
#define ATA_PIO_DRIVE_SLAVE 0x10  /* select slave */

#define ATA_PIO_ERROR_ADDR_MARK_NOT_FOUND 0
#define ATA_PIO_ERROR_TRACK_ZERO_NOT_FOUND 1
#define ATA_PIO_ERROR_ABORTED 2
#define ATA_PIO_ERROR_MEDIA_CHANGE_REQUEST 3
#define ATA_PIO_ERROR_ID_NOT_FOUND 4
#define ATA_PIO_ERROR_MEDIA_CHANGED 5
#define ATA_PIO_ERROR_UNCORRECTABLE_DATA 6
#define ATA_PIO_ERROR_BAD_BLOCK 7

#define ATA_PIO_STATUS_REG_ERR (1 << 0)
#define ATA_PIO_STATUS_IDX (1 << 1)
#define ATA_PIO_STATUS_CORP (1 << 2)
#define ATA_PIO_STATUS_DRQ (1 << 3)
#define ATA_PIO_STATUS_SRV (1 << 4)
#define ATA_PIO_STATUS_DRIVE_FAULT (1 << 5)
#define ATA_PIO_STATUS_DRIVE_RDY (1 << 6)
#define ATA_PIO_STATUS_DRIVE_BUSY (1 << 7)

#define MAX_ATA_CONTROLLER 2
#define MAX_ATA_DEVICES 2

#define DISK_SECTOR_SIZE 512

#define PART_TYPE_EXTENDED 0x5
#define PART_TYPE_FAT16 0x6
#define PART_TYPE_FAT16_LBA 0xe
#define PART_TYPE_FAT32 0xb
#define PART_TYPE_FAT32_LBA 0xc
#define PART_TYPE_LINUX_SWAP 0x82
#define PART_TYPE_LINUX 0x83

typedef enum {
	ATA_DEV_UNKNOWN,
	ATA_DEV_PATA,
	ATA_DEV_SATA,
	ATA_DEV_PATAPI,
	ATA_DEV_SATAPI
} ata_type;

struct ata_device {
	int id;
	ata_type type;
	int isSlave;
	struct ata_controller *ctl;
	int heads;
	int cyls;
	int sectors;
};

struct ata_controller {
	int id;
	int16_t base;
	int16_t dev_ctl;
	int present;
	struct ata_device devices[MAX_ATA_DEVICES];
	struct mutex mutex;
	int last_device_used;
};

struct ata_partition {
	int id;
	uint type;
	uint size;
	uint32_t lba;
	struct ata_device *device;
	struct ata_partition *prev, *next;
};

int ATAInit();
int ATAReadPartitionSector(struct ata_partition *part, int offset, uint nbSector, void *buf);
int ATAReadSector(struct ata_device *dev, int lba, uint nbSector, void *buf);
int ATAWriteSector(struct ata_device *dev, int lba, uint nbSector, void *buf);
struct ata_device *ATAGetDevice(int ctlId, int devId);
struct ata_partition *ATAGetPartitionByType(uint type);
int ATAReadPartition(struct ata_device *dev);
struct ata_partition *ATAGetPartition(int id);
