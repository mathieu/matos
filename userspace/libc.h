#pragma once
#include "assert.h"
#include "stdarg.h"
#include "stdint.h"
#include "stddef.h"
#include "minmax.h"
#include "unistd.h"

#define islower(c) (('a' <= (c)) && ((c) <= 'z'))
#define isupper(c) (('A' <= (c)) && ((c) <= 'Z'))
#define isdigit(c) (('0' <= (c)) && ((c) <= '9'))
#define isspace(c)                                                                            \
    (((c) == ' ') || ((c) == '\t') || ((c) == '\f') || ((c) == '\n') || ((c) == '\r') ||      \
     ((c) == '\v'))
#define isprint(c) ((' ' <= (c)) && ((c) <= '~'))
#define EOF (-1)

/** compares the first @param n bytes (each interpreted as
 *      unsigned char) of the memory areas @param s1 and @param s2.
 */
__attribute__ ((access (read_only, 1, 3), access (read_only, 2, 3))) int memcmp(const void *s1, const void *s2, size_t n);

/**
 * copies n bytes from memory area src to memory area dest. The memory areas may overlap
 */
__attribute__ ((access (write_only, 1, 3), access (read_only, 2, 3))) void *memmove(void *dest, const void *src, size_t n);
__attribute__ ((access (write_only, 1, 3), access (read_only, 2, 3))) void *memcpy(void *dest, const void *src, size_t n);
__attribute__ ((access (write_only, 1, 3))) void *memset(void *s, int c, size_t n);
char *itoa(long long int value, char *str, int base);
void reverse(char s[]);
int strlen(const char s[]);
__attribute__ ((access (read_only, 1, 2))) unsigned int strnlen(const char *s, size_t count);
int strcmp(const char s1[], const char s2[]);
__attribute__ ((access (read_only, 2), access (write_only, 1, 3))) char *strzcpy(char *dst, const char *src, int len);
int puts(const char *str);
int putc(const int c);
int vsnprintf(char *str, size_t size, const char *format, va_list ap) __attribute__ ((__format__ (printf, 3, 0)));
int vprintf(const char *format, va_list ap) __attribute__ ((__format__ (printf, 1, 0)));
int printf(const char *format, ...) __attribute__ ((__format__ (printf, 1, 2)));
void *mmap(void *addr, size_t len, int prot, int flags, char *path);
int munmap(void *addr, size_t len);

int asprintf(char **strp, const char *fmt, ...) __attribute__ ((__format__ (printf, 2, 3)));
int vasprintf(char **strp, const char *fmt, va_list ap) __attribute__ ((__format__ (printf, 2, 0)));

int syscall5(int id, unsigned int arg1, unsigned int arg2, unsigned int arg3,
             unsigned int arg4, unsigned int arg5);
int syscall4(int id, unsigned int arg1, unsigned int arg2, unsigned int arg3,
             unsigned int arg4);
int syscall3(int id, unsigned int arg1, unsigned int arg2, unsigned int arg3);
int syscall2(int id, unsigned int arg1, unsigned int arg2);
int syscall1(int id, unsigned int arg1);
int syscall0(int id);

void _exit(int status);
void helo();
int testSycall5(uint arg1, uint arg2, uint arg3, uint arg4, uint arg5);
char readc();
char getchar();
int readline(char *buf, int size);
int brk(void *addr);
void *sbrk(intptr_t increment);
void *malloc(size_t size);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);
void free(void *ptr);
pid_t gettid(void);
pid_t getpid(void);
pid_t fork();
