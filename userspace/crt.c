#include "libc.h"

void _start()
{
  /* This starter function expects a main() function somewhere */
  extern int main();

  _exit(main());
}
