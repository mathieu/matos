#pragma once
#include "libc.h"

#define assert(p)                                                                             \
    do {                                                                                      \
        if (!(p)) {                                                                           \
            printf("BUG at %s:%d assert(%s)\n", __FILE__, __LINE__, #p);                      \
            _exit(-1);                                                                        \
        }                                                                                     \
    } while (0)

#define assertmsg(p, ...)                                                                     \
    do {                                                                                      \
        if (!(p)) {                                                                           \
            printf("BUG at %s:%d assert(%s)\n", __FILE__, __LINE__, #p);                      \
            printf(__VA_ARGS__);                                                              \
            _exit(-1);                                                                        \
        }                                                                                     \
    } while (0)
