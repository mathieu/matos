#include "math.h"

inline uint32_t log2(const uint32_t x)
{
    uint32_t y;
    // Get the highest set bit
    asm("\tbsr %1, %0\n" : "=r"(y) : "r"(x));
    return y;
}
