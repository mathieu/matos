#pragma once


// Virtual address
typedef unsigned long vaddr_t;

// Physical address
typedef unsigned long paddr_t;

// Userspace vaddr
typedef unsigned long uaddr_t;
