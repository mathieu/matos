#pragma once

/* To keep in sync with PAGING_MEM_* */
#define PROT_EXEC  (1<<3)
#define PROT_READ  (1<<1)
#define PROT_WRITE (1<<2)

#define MAP_SHARED (1 << 0)
#define MAP_PRIVATE (1 << 1)
#define MAP_FIXED (1 << 2)


#define MAP_FAILED (void *)-1
