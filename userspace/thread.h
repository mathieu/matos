#pragma once
#include <stddef.h>

typedef unsigned long int pthread_t;
typedef void *(start_routine)(void *);
int thread_create(pthread_t *thread, start_routine *func, void *arg, size_t stackSize);
/* retval is ignored ATM */
int thread_join(pthread_t thread, void **retval);
