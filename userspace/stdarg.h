#pragma once

//__builtin_va_list could be used instead
typedef char *va_list;
#define va_start(v, l) ((v) = (va_list) & (l) + sizeof(l))
#define va_end(v) ((v) = NULL)
#define va_arg(v, type) (*(type *)(((v) += sizeof(type)) - sizeof(type)))
