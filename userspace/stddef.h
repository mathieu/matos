#pragma once

typedef enum { FALSE = 0, TRUE } bool_t;
#define NULL ((void *)0)

#if __x86_64__
typedef unsigned long size_t;
typedef long ssize_t;
typedef unsigned long int uintptr_t;
typedef long int intptr_t;
#else
typedef unsigned int size_t;
typedef int ssize_t;
typedef unsigned int uintptr_t;
typedef int intptr_t;
#endif
#if __x86_64__
typedef unsigned long size_t;
typedef long ssize_t;
typedef unsigned long int uintptr_t;
typedef long int intptr_t;
#else
typedef unsigned int size_t;
typedef int ssize_t;
typedef unsigned int uintptr_t;
typedef int intptr_t;
#endif

typedef int wchar_t;
