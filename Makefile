# Used to generated .d file that deal with header dependencies
CPPFLAGS = -MMD
AS=nasm
ASFLAGS += -f elf32
LDFLAGS += -m elf_i386
CFLAGS += -m32 -pipe -Wall -Wextra -Werror -ffreestanding -fno-exceptions -fno-pie -fno-tree-vectorize -D__KERNEL__ -fstack-protector-all -mstack-protector-guard=global
#keep .i and .s
#CFLAGS += -save-temps -fverbose-asm
#CFLAGS += -fanalyzer -Wno-analyzer-malloc-leak -Wno-analyzer-out-of-bounds
CXXFLAGS += -m32 -Wall -Wextra -Werror -ffreestanding -fno-exceptions -fno-rtti -fno-pie
DEBUG_FLAGS += -g -Og -DDEBUG -fno-omit-frame-pointer -fno-inline
LIBGCC = $(shell $(CC) -print-libgcc-file-name $(CFLAGS))

QEMU_OPT += -hda disk.img
# Get more information about brutal shutdown
# QEMU_OPT += -d guest_errors,cpu_reset,int
ARCH?=x86
SUBDIRS := core drivers tests arch/$(ARCH)

INCDIRS += $(foreach dir, $(SUBDIRS), -I$(dir))
CPPFLAGS += $(INCDIRS)

asmsrc=
asmobj=$(asmsrc:%.asm=%.o)
gasmsrc=$(wildcard arch/$(ARCH)/*.S arch/$(ARCH)/boot/*.S)
gasmobj=$(gasmsrc:%.S=%.o)
csrc=$(shell find $(SUBDIRS) -type f -name "*.c")#   $(wildcard *.c)
cobj=$(csrc:%.c=%.o)
cinc=$(csrc:%.c=%.i)
deps=$(csrc:%.c=%.d) $(gasmsrc:%.S=%.d)
docsrc=$(wildcard docs/*.md)
docobj=$(docsrc:%.md=%.html)

kernel kernel.debug &: $(asmobj) $(gasmobj) $(cobj) linker.ld
	$(LD) $(LDFLAGS) $(asmobj) $(gasmobj) $(cobj) -o kernel -T linker.ld $(LIBGCC) -Map kernel.map
	objcopy --only-keep-debug kernel kernel.debug
	objcopy --strip-debug  kernel
	objcopy --add-gnu-debuglink=kernel.debug kernel

fd.iso: kernel
	mkdir -p isodir/boot/grub
	cp $< isodir/boot/
	@printf "set timeout=0\nset default=0\nmenuentry \"matos\" {\n\tmultiboot /boot/kernel\n}" > isodir/boot/grub/grub.cfg
	grub-mkrescue -o $@ isodir

userspace: FORCE
	$(MAKE) -C userspace

FORCE:
	@

doc: $(docobj) ## generate documentation

help: ## show this help
# regex for general help
	@sed -ne "s/^##\(.*\)/\1/p" $(MAKEFILE_LIST)
# regex for makefile commands (targets)
	@printf "────────────────────────`tput bold``tput setaf 2` Make Commands `tput sgr0`────────────────────────────────\n"
	@sed -ne "/@sed/!s/\(^[^#?=]*:\).*##\(.*\)/`tput setaf 2``tput bold`\1`tput sgr0`\2/p" $(MAKEFILE_LIST)
# regex for makefile variables
	@printf "────────────────────────`tput bold``tput setaf 4` Make Variables `tput sgr0`───────────────────────────────\n"
	@sed -ne "/@sed/!s/\(.*\)?=\(.*\)##\(.*\)/`tput setaf 4``tput bold`\1:`tput setaf 5`\2`tput sgr0`\3/p" $(MAKEFILE_LIST)

disk.img: disk.sfdisk userspace
	qemu-img create -f raw $@ 32M
	sfdisk $@ < $<
	# Before having filesystem support, just dump the user prog into the first partition
	strip userspace/user -o userspace/user.strip
	$(eval file_size:=$(shell du -b userspace/user.strip|awk '{print $$1}' | xargs printf "%016d" )) \
	sed '1s/^/$(file_size)/' userspace/user.strip | dd  of=disk.img seek=34816 bs=512

# NASM without preprocessing
%.o:%.asm
	$(AS) $(ASFLAGS) -o $@ $<

#GNU GAS ASM with C Preprocessing
%.o: %.S
	$(CC) $(CFLAGS) $(CPPFLAGS) -c "$<" -o "$@"

%.html: %.md
	markdown -o $@ $<

%.i: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -E -c "$<" -o "$@"

test: CFLAGS += -DRUN_TEST   ## Run the OS on qemu with the inner test activated
test: clean kernel disk.img
	qemu-system-x86_64 -no-reboot -kernel kernel -serial stdio -m 32M $(QEMU_OPT)

run:kernel disk.img          ## Run the OS on qemu
	qemu-system-x86_64 -no-reboot -kernel $< -serial stdio $(QEMU_OPT)

debug: CFLAGS += $(DEBUG_FLAGS) ## Run the OS on qemu and attach a debugger to it (may need a clean before to have the debug symbols)
debug: CXXFLAGS += $(DEBUG_FLAGS)
debug:kernel kernel.debug disk.img
	gdb -q -x debug.gdb

isodebug: CFLAGS += $(DEBUG_FLAGS) ## Same than previous but kernel is loaded by grub. So, for example, we can access the elf debug info
isodebug: CXXFLAGS += $(DEBUG_FLAGS)
isodebug:fd.iso disk.img
	gdb -q -x debug.iso.gdb

debug_test: CFLAGS += $(DEBUG_FLAGS) -DRUN_TEST
debug_test: debug

screenshot: ## Take a screenshot of the qemu window
	shutter --window=qemu -o screenshot_1.png -e && zopflipng screenshot_1.png screenshot_1.png

clean:
	$(RM) kernel $(asmobj) $(gasmobj) $(cobj) $(deps) $(cinc) fd.iso kernel.debug kernel.map $(docobj)
	$(RM) -r isodir
	$(MAKE) -C userspace clean

.PHONY:
	userspace screenshot

ifneq ($(MAKECMDGOALS),clean)
-include $(deps)
endif
