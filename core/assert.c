#include "assert.h"
#include "klibc.h"
#include "stack.h"

#define assert(p)                                                                             \
    do {                                                                                      \
        if (!(p)) {                                                                           \
            printf("BUG at %s:%d assert(%s)\n", __FILE__, __LINE__, #p);                      \
            printStackTrace(3);                                                               \
            while (1) {                                                                       \
            }                                                                                 \
        }                                                                                     \
    } while (0)

#define assertmsg(p, ...)                                                                     \
    do {                                                                                      \
        if (!(p)) {                                                                           \
            printf("BUG at %s:%d assert(%s)\n", __FILE__, __LINE__, #p);                      \
            printf(__VA_ARGS__);                                                              \
            printStackTrace(3);                                                               \
            while (1) {                                                                       \
            }                                                                                 \
        }                                                                                     \
    } while (0)
