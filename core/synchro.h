#pragma once
#include <wait.h>

int mutexInit(struct mutex *);
int mutexFree(struct mutex *);
int mutexLock(struct mutex *);
int mutexUnlock(struct mutex *);

int semInit(struct semaphore *, int initVal);
int semFree(struct semaphore *);
int semDown(struct semaphore *, unsigned long timeout);
int semTryDown(struct semaphore *);
int semUp(struct semaphore *);
