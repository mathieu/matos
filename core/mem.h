#pragma once
#include "stdint.h"
#include "types.h"

/**
 * Physical Page management
 */

#define PAGE_SHIFT 12U
#define PAGE_SIZE (1U << PAGE_SHIFT)
#define PAGE_MASK  (PAGE_SIZE - 1)


// Defined in linker.ld script
extern char  __ld_kernel_begin;
extern char  __ld_kernel_end;

struct phyMemDesc {
    paddr_t phy_addr;
    unsigned long ref;
    struct phyMemDesc *next, *prev;
};

/**
 * Initi Physical Page management subsystem
 **/
int memSetup(paddr_t upperMem, paddr_t * firstUsed, paddr_t *lastUsed);

/**
 * Declare a physical region to be managed by the physica page subsystem
 **/
int memAddBank(paddr_t bottomMem, paddr_t topMem, int isFree);

/**
 * Request @params nbPage free physical pages
 **/
paddr_t allocPhyPage(uint nbPage);

/**
 * Decrement the nb of user of a given physical page
 **/
int unrefPhyPage(paddr_t addr);

/**
 * Increment the nb of user of a given physical page
 **/
int refPhyPage(paddr_t addr);

/**
 * Return the nb of user of a given physical page
 **/
int getRefPage(paddr_t addr);

/**
 * Return the number of physical allocated pages
 **/
unsigned long getNbAllocatedPage(void);
void memGetStat(uint *free, uint *used);
