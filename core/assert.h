#pragma once
#include "klibc.h"
#include "stack.h"

#define assert(p)                                                                             \
    do {                                                                                      \
        if (!(p)) {                                                                           \
            printf("BUG at %s:%d assert(%s)\n", __FILE__, __LINE__, #p);                      \
            printStackTrace(3);                                                               \
            while (1) {                                                                       \
            }                                                                                 \
        }                                                                                     \
    } while (0)

#define assertmsg(p, ...)                                                                     \
    do {                                                                                      \
        if (!(p)) {                                                                           \
            printf("BUG at %s:%d assert(%s)\n", __FILE__, __LINE__, #p);                      \
            printf(__VA_ARGS__);                                                              \
            printStackTrace(3);                                                               \
            while (1) {                                                                       \
            }                                                                                 \
        }                                                                                     \
    } while (0)

#define panic(fmt, args...)                                                                   \
    do {                                                                                      \
        asm volatile("cli");                                                                  \
        printf("PANIC at %s:%d\n " fmt "", __FILE__, __LINE__);                                \
        printf("PANIC: " fmt "\n", ##args);                               \
        printStackTrace(3);                                                                   \
        while (1)                                                                             \
            asm volatile("hlt");                                                              \
        __builtin_unreachable();                                                              \
    } while (0)
