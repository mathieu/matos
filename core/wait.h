#pragma once

#include "thread.h"

struct wait_queue {
    struct thread *thread;
    struct wait_queue *next;
    struct wait_queue *prev;
};

#define __WAITQUEUE_INITIALIZER(name)                                                         \
    {                                                                                         \
        .thread = NULL, .next = NULL, .prev = NULL                                            \
    }

#define DECLARE_WAITQUEUE(name)					\
	struct wait_queue name = __WAITQUEUE_INITIALIZER(name)

int waitQueueInit(struct wait_queue *);
int waitQueueFree(struct wait_queue *);
int wait(struct wait_queue *);
int waitTimeout(struct wait_queue *wq, unsigned long msec);
int waitUp(struct wait_queue *);
int waitUpNb(struct wait_queue *wq, unsigned int nbThread);

#define wait_event(wq, condition)                                                             \
    do {                                                                                      \
        if (condition)                                                                        \
            break;                                                                            \
        wait(wq);                                                                             \
    } while (1)

struct semaphore {
    int count;
    struct wait_queue wait;
};

struct mutex {
    struct thread *owner;
    struct wait_queue *wait;
};
