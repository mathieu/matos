#pragma once
#include "stdarg.h"
#include "wait.h"

typedef struct ringbuffer_instance_t *ringbuffer_t;

ringbuffer_t ringbufferCreate(uint32_t capacity);
uint32_t ringbufferCapacity(ringbuffer_t instance);
uint32_t ringbufferUsed(ringbuffer_t instance);
bool_t ringbufferEnqueue(ringbuffer_t instance, uint8_t item);
bool_t ringbufferDequeue(ringbuffer_t instance, uint8_t *item);
void ringbufferDestroy(ringbuffer_t instance);
void ringbufferDebug(ringbuffer_t instance);
bool_t ringbufferIsEmpty(ringbuffer_t instance);
bool_t ringbufferIsFull(ringbuffer_t instance);

typedef struct locked_ringbuffer_instance_t *locked_ringbuffer_t;

locked_ringbuffer_t lringbufferCreate(uint32_t capacity);
uint32_t lringbufferCapacity(locked_ringbuffer_t instance);
uint32_t lringbufferUsed(locked_ringbuffer_t instance);
bool_t lringbufferEnqueue(locked_ringbuffer_t instance, uint8_t item);
bool_t lringbufferDequeue(locked_ringbuffer_t instance, uint8_t *item);
void lringbufferDestroy(locked_ringbuffer_t instance);
void lringbufferDebug(locked_ringbuffer_t instance);
bool_t lringbufferIsEmpty(locked_ringbuffer_t instance);
bool_t lringbufferIsFull(locked_ringbuffer_t instance);
