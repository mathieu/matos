#pragma once
#include "mmuContext.h"
#include "process.h"
#include "stddef.h"
#include "stdint.h"
#include "types.h"

struct uAddrSpace;
struct uAddrVirtualReg;

#define UA_MAP_SHARED (1 << 0)
#define UA_MAP_PRIVATE (1 << 1)
#define UA_MAP_FIXED (1 << 2)

// TODO : move this struct to .c and add accessors
struct uAddrVirtualReg {
    uaddr_t addr;
    size_t size;
    int right;       // PAGING_MEM_*
    uint32_t offset; // in the mappedRessource
    uint flags;      // UA_MAP_*

    struct mappedRessource *res;
    struct uAddrVirtualReg *nextInAddrSpace, *prevInAddrSpace;
    struct uAddrVirtualReg *nextInMappedRes, *prevInMappedRes;
};

struct mappedRessourceOps {
    int (*open)(struct uAddrVirtualReg *vreg);
    int (*close)(struct uAddrVirtualReg *vreg);
    int (*unmap)(struct uAddrVirtualReg *vreg, uaddr_t addr, size_t size);
    int (*nopage)(struct uAddrVirtualReg *vreg, uaddr_t addr,
                  int right); // Called by the pageflt handler when the page is missing
};

struct mappedRessource {
    int allowedRight; // PAGING_MEM_*
    struct mappedRessourceOps *ops;
    struct uAddrVirtualReg *listVirtualReg;

    int (*mmap)(struct uAddrSpace *as, uaddr_t *uaddr, size_t size, uint32_t rights,
                uint32_t flags);
    void *customData;
    int (*onResMapped)(
        struct uAddrVirtualReg *reg); // Callback for when the ressourced get mapped
};

struct uAddrSpace *uAddrSpaceCreate(struct process *proc);
int uAddrSpaceDelete(struct uAddrSpace *addr);
struct mmu_context *uAddrSpaceGetMMUContext(struct uAddrSpace *addr);
int uAddrSpaceSetHeap(struct uAddrSpace *as, uaddr_t addr, size_t size);
int uAddrSpaceHeapCheckNAlloc(struct uAddrSpace *as, vaddr_t addr);
uaddr_t sysBrk(struct uAddrSpace *as, uaddr_t newHeapTop);

int uAddrSpaceMmap(struct uAddrSpace *as, uaddr_t *uaddr, size_t size, uint32_t rights,
                   uint32_t flags, struct mappedRessource *res, uint32_t offset);
int uAddrSpaceUnmap(struct uAddrSpace *as, uaddr_t uaddr, size_t size);
int uAddrSpaceSolvePageFault(struct uAddrSpace *as, vaddr_t faultAddr, int isWriteAccess);
struct uAddrSpace *uAddrSpaceDuplicate(struct process *origin, const struct process *model);
