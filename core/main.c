#include "alloc.h"
#include "allocArea.h"
#include "ata.h"
#include "elf.h"
#include "exception.h"
#include "gdt.h"
#include "idt.h"
#include "interrupt.h"
#include "irq.h"
#include "kernel.h"
#include "keyboard.h"
#include "klibc.h"
#include "mem.h"
#include "mmuContext.h"
#include "multiboot.h"
#include "paging.h"
#include "pit.h"
#include "process.h"
#include "serial.h"
#include "stack.h"
#include "swintr.h"
#include "thread.h"
#ifdef RUN_TEST
#include "test.h"
#endif
#include "time.h"
#include "types.h"
#include "vga.h"
#include "zero.h"

#define CHECK_FLAG(flags, bit) ((flags) & (1 << (bit)))

void idleThread(void *arg)
{
    (void)arg;
    while (1) {
        VGAPrintf(GREEN, BLACK, 0, VGA_HEIGHT - 1, "%d allocated %d, mapped %d", (jiffies / HZ), getNbAllocatedPage(), getNbMappedPage());
        threadYield();
    }
}


#define FILE_HEADER_SIZE   16
#define FILE_MAX_SIZE   64 // In nb of sectors
void loadUserSpace()
{
    struct ata_partition *part = ATAGetPartitionByType(PART_TYPE_LINUX);

    if (part == NULL) {
        printf("No user partition found\n");
        return;
    }

    char *buf = malloc(FILE_MAX_SIZE * DISK_SECTOR_SIZE);
    if (buf == NULL) {
        printf("ENOMEM\n");
        return;
    }

    if (ATAReadPartitionSector(part, 0, 1, buf)) {
        printf("Fail to read from disk\n");
        return;
    }

    int sectorToRead;
    {
        char size[FILE_HEADER_SIZE + 1];
        memcpy(size, buf, FILE_HEADER_SIZE);
        size[FILE_HEADER_SIZE] = '\0';
        int sizeInt            = atoi(size);
        sectorToRead           = DIV_ROUND_UP(sizeInt, DISK_SECTOR_SIZE) - 1;
    }

    if (sectorToRead > FILE_MAX_SIZE - 1 || sectorToRead <= 0) {
        printf("Invalid file size: %d (sector)\n", sectorToRead);
        return;
    }

    if (ATAReadPartitionSector(part, 1, sectorToRead,
                               buf + DISK_SECTOR_SIZE)) {
        printf("Fail to read from disk\n");
        return;
    }

    {
        struct process *proc = processCreate("init");
        struct uAddrSpace *as = processGetAddrSpace(proc);

        threadChangeCurrentContext(processGetMMUContext(proc));
        uaddr_t prog = loadElfProg(buf + FILE_HEADER_SIZE, proc);
        if (prog == (uaddr_t)NULL) {
            free(buf);
            return;
        }

        // Alloc user stack
        size_t stackSize = PAGE_SIZE * 2;

        // It will be better to use PAGING_TOP_USER_ADDRESS - stackSize + 1 but, in this case,
        // stack + stack size tend to overflow and produce various bug.
        uaddr_t stack = ALIGN_DOWN(PAGING_TOP_USER_ADDRESS - stackSize, PAGE_SIZE);

        assert(!zeroMmap(as, &stack, stackSize,
                         PAGING_MEM_USER | PAGING_MEM_WRITE | PAGING_MEM_READ, 0));

        threadCreateUser("init_th", proc, prog, 0, 0, stack + stackSize - 4, NULL);
        processUnref(proc);
        threadChangeCurrentContext(NULL);
    }

    free(buf);
}

// Multiboot information available here :
// https://www.gnu.org/software/grub/manual/multiboot/multiboot.html#kernel_002ec
// https://www.gnu.org/software/grub/manual/multiboot/html_node/Boot-information-format.html#Boot%20information%20format
void kmain(unsigned long magic, unsigned long addr)
{
    unsigned long upperMemKB = 0;
    int memMapAvailable      = 0;
    paddr_t lastUsedByMem;
    paddr_t firstUsedByMem;

    VGASetup(BLACK, GREEN);

    printf("[Setup] Interruptions\n");
    gdtSetup();
    idtSetup();
    irqSetup();
    pitSetup(HZ);

    // https://www.gnu.org/software/grub/manual/multiboot/multiboot.html#Boot-information-format
    if (magic == MULTIBOOT_BOOTLOADER_MAGIC) { // Get loaded by Grub with mutliboot version 1
        multiboot_info_t *mbi = (multiboot_info_t *)addr;
        /* Are mem_* valid? */
        if (CHECK_FLAG(mbi->flags, 0)) {
            printf("mem_lower = %dKiB, mem_upper %dKiB\n", mbi->mem_lower, mbi->mem_upper);
            upperMemKB = mbi->mem_upper;
        }

        /* Is boot_device valid? */
        if (CHECK_FLAG(mbi->flags, 1)) {
            int disk  = mbi->boot_device >> 24 & 0xff;
            int part1 = mbi->boot_device >> 16 & 0xff;
            int part2 = mbi->boot_device >> 8 & 0xff;
            int part3 = mbi->boot_device & 0xff;
            printf("boot_device = 0x%x (0x0 for floppy 0x80 for disk) part %d %d %d\n", disk,
                   part1, part2, part3);
        }

        if (CHECK_FLAG(mbi->flags, 3)) {
            if (mbi->mods_count > 0) {
                multiboot_module_t *mod;
                uint32_t i;

                printf("mods_count = %d, mods_addr = 0x%x\n", (int)mbi->mods_count,
                       (int)mbi->mods_addr);
                for (i = 0, mod = (multiboot_module_t *)mbi->mods_addr; i < mbi->mods_count;
                     i++, mod++)
                    printf(" mod_start = 0x%x, mod_end = 0x%x, cmdline = %s\n",
                           (unsigned)mod->mod_start, (unsigned)mod->mod_end,
                           (char *)mod->cmdline);
            }
        }

        if (CHECK_FLAG(mbi->flags, 5)) {
            stackSymbolSetup(mbi);
        }
        if (CHECK_FLAG(mbi->flags, 6)) {
            memMapAvailable = 1;
        }

        if (CHECK_FLAG(mbi->flags, 9)) {
            printf("Loaded by %s. ", (char *)mbi->boot_loader_name);
        }

        /* Is the command line passed? */
        if (CHECK_FLAG(mbi->flags, 2)) {
            printf("cmdline = %s\n", (char *)mbi->cmdline);
        }

        if (CHECK_FLAG(mbi->flags, 12)) {
            printf("Re configure Framebuffer from 0x%llx size (%dx%d) pitch %d bpp %d\n",
                   mbi->framebuffer_addr, mbi->framebuffer_width, mbi->framebuffer_height,
                   mbi->framebuffer_pitch, mbi->framebuffer_bpp);
            VGAConfigure(mbi->framebuffer_addr, mbi->framebuffer_width,
                         mbi->framebuffer_height);
        }
    }

    if (upperMemKB == 0) {
        printf("Cannot get upper phy mem bound. Using default value 32MB\n");
        upperMemKB = 32 * 1024;
    }

    printf("[Setup] Mem\n");

    memSetup(upperMemKB, &firstUsedByMem, &lastUsedByMem);

    if (memMapAvailable) {
        multiboot_info_t *mbi             = (multiboot_info_t *)addr;
        struct multiboot_mmap_entry *mmap = (struct multiboot_mmap_entry *)mbi->mmap_addr;
        uint size = mbi->mmap_length / sizeof(struct multiboot_mmap_entry);

        pr_devel("mmap buffer at 0x%x with %d entries\n", mbi->mmap_addr, size);

        for (uint i = 0; i < size; i++) {
            printf("   base_addr 0x%llx, length = 0x%llx, type = 0x%x\n", mmap[i].addr,
                   mmap[i].len, (uint32_t)mmap[i].type);
            // Consider low memory taken (https://wiki.osdev.org/Detecting_Memory_(x86). For
            // example by VGA
            // Turns out linux and windows do the same !
            // https://lore.kernel.org/lkml/MWHPR21MB159330952629D36EEDE706B3D7379@MWHPR21MB1593.namprd21.prod.outlook.com/
            if (mmap[i].addr < 0x100000) {
                printf("    -> skipping\n");
                continue;
            }
            memAddBank(max(mmap[i].addr, (multiboot_uint64_t)lastUsedByMem),
                       mmap[i].addr + mmap[i].len, mmap[i].type == MULTIBOOT_MEMORY_AVAILABLE);
        }
    } else {
        printf("Cannot get memory Mapping information, using default value\n");
        memAddBank(lastUsedByMem, upperMemKB * 1024, 1);
    }
    printf("%ld pages used by kernel(0x%lx->0x%lx))",
           (lastUsedByMem - firstUsedByMem) / PAGE_SIZE, firstUsedByMem, lastUsedByMem);
    printf(" (%lu pages for MM)\n", (lastUsedByMem - (paddr_t)&__ld_kernel_end) / PAGE_SIZE);

#ifdef RUN_TEST
    testPhymem();
#endif
    printf("[Setup] Pagination\n");
    pagingSetup(firstUsedByMem, lastUsedByMem);
    VGAMap();


    printf("[Setup] HW interrupts\n");
    // Enabling the HW interrupts
    exceptionSetup();
    asm volatile("sti\n");

    printf("[Setup] Serial link (115200)\n");
    serialSetup(115200);

    printf("[Setup] allocation system\n");
    areaInit(firstUsedByMem, lastUsedByMem, _stack_bottom, _stack_top);

    printf("[Setup] IRQ handlers\n");
    keyboardSetup();
    irqSetRoutineWrapped(IRQ_KEYBOARD, keyboard_do_irq);

    mmuContextSetup();
    cpu_context_subsystem_setup();

    printf("[Setup] thread system\n");
    threadSetup(_stack_bottom, (_stack_top - _stack_bottom + 1));
    threadCreate("idle ", idleThread, NULL);
    processSetup();
    syscallSetup();

    irqSetRoutine(IRQ_TIMER, pit_handler);

    ATAInit();
#ifdef RUN_TEST
    run_test();
#endif
    printf("\nSystem init done: ");

    {
        uint free, used;
        memGetStat(&free, &used);
        printf("%dKB free %dKB Used\n", free * (PAGE_SIZE / 1024), used * (PAGE_SIZE / 1024));
    }

    loadUserSpace();
    // There is no real caller behind this point
    // So finish this by ourself
    threadExit();
}
