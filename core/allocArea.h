#pragma once
#include "paging.h"
#include "stdarg.h"
#include "stdint.h"

/* Pure Virtual Memory Allocation */

// areaAlloc map vmem to phy pages
#define AREA_PHY_MAP (1<<0)
#define AREA_MEM_TOP  PAGING_MIRROR_VADDR

/**
 * Initialize the Area subsystem
 **/
void areaInit(vaddr_t firstMemUsed, vaddr_t lastUsed, vaddr_t stack_bottom, vaddr_t stack_top);

/**
 * Request a virtual memory area of @param nbPages
 **/
vaddr_t areaAlloc(unsigned int nbPages, uint32_t flags);

/**
 * Free a virtual area
 **/
int areaFree(vaddr_t addr);

/**
 * Remove an area from the "free" ones but do not add it into used ones.
 * This area should be latter added with areaAdd.
 * Used by malloc to avoid recursivity issue
 **/
vaddr_t areaBook(unsigned int nbPages, uint32_t flags);

/**
 * Declare a virtual region to be managed by the subsytem
 */
int areaAdd(vaddr_t begin, vaddr_t end, int isFree);
