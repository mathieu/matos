#pragma once
#include "assert.h"
#include "stdarg.h"
#include "stddef.h"
#include "minmax.h"

#define islower(c) (('a' <= (c)) && ((c) <= 'z'))
#define isupper(c) (('A' <= (c)) && ((c) <= 'Z'))
#define isdigit(c) (('0' <= (c)) && ((c) <= '9'))
#define isspace(c)                                                                            \
    (((c) == ' ') || ((c) == '\t') || ((c) == '\f') || ((c) == '\n') || ((c) == '\r') ||      \
     ((c) == '\v'))
#define isprint(c) ((' ' <= (c)) && ((c) <= '~'))

/** compares the first @param n bytes (each interpreted as
 *      unsigned char) of the memory areas @param s1 and @param s2.
 */
__attribute__ ((access (read_only, 1, 3), access (read_only, 2, 3))) int memcmp(const void *s1, const void *s2, size_t n);

/**
 * copies n bytes from memory area src to memory area dest. The memory areas may overlap
 */
__attribute__ ((access (write_only, 1, 3), access (read_only, 2, 3))) void *memmove(void *dest, const void *src, size_t n);
__attribute__ ((access (write_only, 1, 3), access (read_only, 2, 3))) void *memcpy(void *dest, const void *src, size_t n);
__attribute__ ((access (write_only, 1, 3))) void *memset(void *s, int c, size_t n);
char *itoa(long long int value, char *str, int base);
int atoi(const char *str);
void reverse(char s[]);
int strlen(const char s[]);
__attribute__ ((access (read_only, 1, 2))) unsigned int strnlen(const char *s, size_t count);
int strcmp(const char s1[], const char s2[]);
__attribute__ ((access (read_only, 2), access (write_only, 1, 3))) char *strzcpy(char *dst, const char *src, int len);
int puts(const char *str);
int putc(const char str);
int vsnprintf(char *str, size_t size, const char *format, va_list ap) __attribute__ ((__format__ (printf, 3, 0)));
int vprintf(const char *format, va_list ap) __attribute__ ((__format__ (printf, 1, 0)));
int printf(const char *format, ...) __attribute__ ((__format__ (printf, 1, 2)));

// Could be used after malloc is available
int asprintf(char **strp, const char *fmt, ...) __attribute__ ((__format__ (printf, 2, 3)));
int vasprintf(char **strp, const char *fmt, va_list ap) __attribute__ ((__format__ (printf, 2, 0)));

/*
 * Dummy printk for disabled debugging statements to use whilst maintaining
 * gcc's format checking.
 */
#define no_printf(fmt, ...)                                                                   \
    ({                                                                                        \
        if (0)                                                                                \
            printf(fmt, ##__VA_ARGS__);                                                       \
        0;                                                                                    \
    })

#ifndef pr_fmt
#define pr_fmt(fmt) fmt
#endif

#ifdef DEBUG
#define pr_devel(fmt, ...) printf(pr_fmt(fmt), ##__VA_ARGS__)
#else
#define pr_devel(fmt, ...) no_printf(pr_fmt(fmt), ##__VA_ARGS__)
#endif

#define pr_info(fmt, ...) printf(pr_fmt(fmt), ##__VA_ARGS__)
#define pr_err(fmt, ...) {printf(pr_fmt(fmt), ##__VA_ARGS__); assert(0);}
