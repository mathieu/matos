#pragma once
#include "paging.h"
#include "stddef.h"
#include "types.h"

/** Arbitrary size allocator **/

/*
 * Initialize malloc system
 */
int allocSetup(size_t sizeOfArea, vaddr_t *areaAddr, vaddr_t *descAddr, vaddr_t *entryAddr);

/*
 * Add Slice for some simple/commun size
 */

int allocPopulate();
/*
 * Allow malloc to allocate elements of this precise size.
 * Otherwise the allocation will be in the closest biggest pool.
 * */
int allocBookSlab(size_t size, size_t sizeSlab, int selfContained, int neverEmpty);

void *malloc(size_t size);
void *zalloc(size_t size);
void free(void *ptr);

/* Stuct definition shared for test purpose
 */
struct slabEntry {
    vaddr_t page;
    void *freeEl;
    size_t size;//of the allocated page
    bool_t full;//TODO replace by freeEl == NULL
    struct slabEntry *next;
    struct slabEntry *prev;
};

struct slabDesc {
    struct slabEntry slab;
    size_t size;
    bool_t neverEmpty;
    struct slabDesc *next;
    struct slabDesc *prev;
};
