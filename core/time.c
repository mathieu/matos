#include "time.h"
#include "stddef.h"
#include <stdint.h>

unsigned long volatile jiffies = INITIAL_JIFFIES;

unsigned int jiffies_to_msecs(const unsigned long j)
{
    return (1000L / HZ) * j;
}

unsigned int jiffies_to_usecs(const unsigned long j)
{
    return (1000000L / HZ) * j;
}

unsigned long msecs_to_jiffies(const unsigned int m)
{
#if HZ >= 1000 && !(HZ % 1000)
    return m * (HZ / 1000L);
#else
    // This could overflow
    return (m * HZ) / 1000L;
#endif
}

unsigned long usecs_to_jiffies(const unsigned int u)
{
    // This could overflow
    return (u * HZ) / 1000000L;
}

#include <x86intrin.h>
inline uint64_t read_cycle_counter()
{
        uint64_t tsc = __rdtsc();
        return tsc;
}
