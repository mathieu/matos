#pragma once
#ifdef __KERNEL__
#include "cpu_context.h"
#endif

#define SYSCALL_ID_EXIT 1
#define SYSCALL_ID_HELO 2
#define SYSCALL_ID_PUTC 3
#define SYSCALL_ID_READ 4
#define SYSCALL_ID_TEST 5
#define SYSCALL_ID_BRK 6
#define SYSCALL_ID_MMAP 7
#define SYSCALL_ID_MUNMAP 8
#define SYSCALL_ID_NEW_THREAD 9
#define SYSCALL_ID_USLEEP 10
#define SYSCALL_ID_GETPID 11
#define SYSCALL_ID_GETTID 12
#define SYSCALL_ID_THREAD_JOIN 13
#define SYSCALL_ID_FORK 14

#ifdef __KERNEL__
int syscallExecute(int syscallId, const struct cpu_state *user_ctx);
#endif
