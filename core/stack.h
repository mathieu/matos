#pragma once
#include "multiboot.h"

#include "types.h"
extern vaddr_t _stack_bottom;
extern vaddr_t _stack_top;
void printStackTrace(unsigned int maxFrame);
void stackSymbolSetup(multiboot_info_t *mbi);
