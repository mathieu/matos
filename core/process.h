#pragma once
#include "types.h"

#define PROCESS_NAME_MAX_LENGTH 32
typedef unsigned long int pid_t;

struct process;
struct thread;

int processSetup();
struct process *processCreate(char *name);
int processCountThread(struct process *proc);
void processListPrint();
int processRef(struct process *proc);
int processUnref(struct process *proc);
int processSetName(struct process *proc, char *name);
char *processGetName(struct process *proc);
int processAddThread(struct process *proc, struct thread *th);
int processRemoveThread(struct thread *th);
struct mmu_context *processGetMMUContext(struct process *th);
struct uAddrSpace *processGetAddrSpace(const struct process *proc);
int processInitHeap(struct process *proc, uaddr_t lastUserAddr);
pid_t processGetId(struct process *proc);
pid_t processGetNextTid(struct process *proc);
int processJoinThread(struct process *proc, pid_t tid);
struct process *processDuplicateCurrent(void);
