#include "irq.h"
#include "thread.h"
#include "list.h"
#include "wait.h"

static struct wait_queue *waitQueues = NULL;

int waitQueueInit(struct wait_queue *wq)
{
    uint32_t flags;
    list_init(wq->thread);
    disable_IRQs(flags);
    list_add_tail(waitQueues, wq);
    restore_IRQs(flags);
    return 0;
}

int waitQueueFree(struct wait_queue *wq)
{
    uint32_t flags;
    disable_IRQs(flags);
    list_delete(waitQueues, wq);
    restore_IRQs(flags);
    return 0;
}

int waitUp(struct wait_queue *wq)
{
    struct thread *th;
    uint32_t flags;

    disable_IRQs(flags);
    list_collapse(wq->thread, th)
    {
        threadAddThread(th);
    }

    restore_IRQs(flags);

    return 0;
}

int waitUpNb(struct wait_queue *wq, unsigned int nbThread)
{
    struct thread *th;
    uint32_t flags;

    disable_IRQs(flags);
    list_collapse(wq->thread, th)
    {
        threadAddThread(th);
        if(nbThread){
            nbThread--;
            if(!nbThread)
                break;
        }
    }

    restore_IRQs(flags);

    return 0;
}

int wait(struct wait_queue *wq)
{
    return waitTimeout(wq, 0);
}

int waitTimeout(struct wait_queue *wq, unsigned long msec)
{
    struct thread *current, *next;
    uint32_t flags;
    int ret;

    disable_IRQs(flags);

    current        = getCurrentThread();
    current->state = WAITING;
    next           = threadSelectNext();
    threadUnsched(current);

    list_add_tail(wq->thread, current);
    ret = threadWait(current, next, msec);

    restore_IRQs(flags);
    return ret;
}
