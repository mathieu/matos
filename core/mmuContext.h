#pragma once
#include "stddef.h"

struct mmu_context;

int mmuContextSetup();
struct mmu_context *mmuContextCreate();
int mmuContextSyncKernelPDE(int pdEntry, void *pde, size_t pdeSize);
int mmuContextSwitch(struct mmu_context *ctx);
struct mmu_context *mmuContextGetCurrent();
int mmuContextRef(struct mmu_context *ctx);
int mmuContextUnref(struct mmu_context *ctx);
struct mmu_context *mmuContextDuplicate(const struct mmu_context *ori);
