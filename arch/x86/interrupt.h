#pragma once
#include "stdarg.h"
#include "cpu_context.h"

// c.f. intel software-developer-vol-1 6.4.1
struct interrupt_frame {
    /* Stacked by the CPU */
    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
} __attribute__((packed));

// IRQ
void pit_handler(struct interrupt_frame *frame);
