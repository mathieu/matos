#pragma once
#include "types.h"

/** Frontier between kernel and user space virtual addresses */
#define PAGING_BASE_USER_ADDRESS (0x40000000) /* 1GB (must be 4MB-aligned) */
#define PAGING_TOP_USER_ADDRESS  (0xFFFFFFFF) /* 4GB - 1B */
#define PAGING_USER_SPACE_SIZE   (0xc0000000) /* 3GB */

/** Length of the space reserved for the mirroring in the kernel
    virtual space */
#define PAGING_MIRROR_SIZE  (PAGE_SIZE << 10)  /* 1 PD = 1024 Page Tables = 4MB */

/** Virtual address where the mirroring takes place */
#define PAGING_MIRROR_VADDR \
   (PAGING_BASE_USER_ADDRESS - PAGING_MIRROR_SIZE)

#define PAGING_MEM_USER (1U << 0)
#define PAGING_MEM_READ (1U << 1)
#define PAGING_MEM_WRITE (1U << 2)
#define PAGING_MEM_EXEC (1U << 3)

int pagingSetup(paddr_t lowerKernelAddr, paddr_t upperKernelAddr);

int pageMap(vaddr_t vaddr, paddr_t paddr, int flags);
int pageUnmap(vaddr_t vaddr);
int pageChangePermission(vaddr_t vaddr, int flags);
unsigned long getNbMappedPage(void);

int pagingSetCurrentPDPaddr(paddr_t paddrPD);
paddr_t pagingGetPaddr(vaddr_t vaddr);
paddr_t pagingGetCurrentPDPaddr();
int pagingCopyKernelSpace(vaddr_t destVaddrPD, paddr_t destPaddrPD, vaddr_t srcVaddrPD);
int pagingCopyUserSpace(vaddr_t dstVaddrPD, vaddr_t srcVaddrPD);
int pagingClearUserContext(vaddr_t vaddr_PD);
int pagingIsPresent(uaddr_t vaddr);
int pagingTrySolveCOW(vaddr_t vaddr);
