#include "assert.h"
#include "swintr.h"
#include "irq.h"
#include "idt.h"

extern void syscallHandler();

int syscallSetup(){
    uint32_t flags, ret;

    disable_IRQs(flags);
    ret = idt_set_handler(SYSCALL_INTR_NB, (vaddr_t)syscallHandler, 3);
    restore_IRQs(flags);

    assert(ret == 0);
    return ret;
}

