# Introduction

Matos is a simple OS.

It's targetting x86 and inspirated by http://sos.enix.org/fr/PagePrincipale

![screenshot](https://git.mathux.org/mathieu/matos/raw/branch/master/screenshot_1.png)

# Dependencies

To generate iso image

 * `mtools xorriso (which is libisoburn on ArchLinux)`
 * gcc for 32bits (e.g. gcc-multilib for ubuntu) >= 11
 * sfdisk (util-linux)

# Run it

 `make run`

 or

 `make fd.iso && qemu-system-x86_64 -cdrom fd.iso`

 you can also test it

 `make test`

# Debug

gdb could be launch with debug symbols using :

 `make debug`

Serial log will be saved in the `serialOut` file.

Then you can check some matos specific commands or pretty printing with

 `help user-defined`
 `info pretty-printer` (Should contains matos_pretty_printers)

# Change Disk partitions

You can either modify the disk.sfdisk file

or

modify the disk image with your favorit tool (e.g. gparted) and re-generate disk.sfdisk

 sfdisk -d > disk.sfdisk

# Multiboot

In arch/x86/boot, you can choose the assembly used to start the C part by removing the .opt extension

* boot.asm: intel syntax with multiboot support
* boot.S: GNU As syntax with multiboot support
* boot_multiboot2.S: GNU As syntax with multiboot2 support

/!\ multiboot2 is not supported by qemu for -kernel option (https://gitlab.com/qemu-project/qemu/-/issues/389).
So you may have to use the fd.iso image and modify the grub configuration from `multiboot /boot/kernel` to `multiboot2 /boot/kernel`

# Features

 * VGA Driver
 * keyboard Driver
 * Serial Driver
 * Timer
 * Virtual memory
 * Allocator
 * Multithread (Full preempt)
 * Mutex
 * ATA
 * basic userspace with syscall

# TODO
 * ringbuffer protected by mutex
 * blocking read
