# System Startup

The Matos operating system is supposed to be started unsing a bootloader that is [multiboot](https://www.gnu.org/software/grub/manual/multiboot/multiboot.html) compatible.
As such, it can receive some information about the host such as memory size, layout, video mode supported ...

When the booloader is ready, it will call the `_start` function as it is described as the entry point by the `linker.ld`.
The `_start` function is defined in ASM under the `arch/$ARCH/boot/boot.S` directory.
It will setup the stack and few other things like the bss section before calling the `kmain()` function from `core/main.c`
