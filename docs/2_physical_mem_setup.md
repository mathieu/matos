# Physical memory setup

The Physical memory definition stand in the `mem.h` file.
To keep track of it, the memory is split in chunk of `PAGE_SIZE`. The chunks are then represented by the `struct phyMemDesc` structure.

This structure is a double-linked list allowing us to hold some information on every page like its address and the number of time it is used. Yes, the same physical page could be shared by several different sub-system. 

The system keep track of 2 such linked list. One with the allocated/used (`phyUsedPage`) page. One with the free one (`phyFreePage`).

## Initial `struct phyMemDesc` allocation and setup

Just after system startup, there is no memory allocator setup, and yet we have to store the information about all the physical pages.

For example, if the system was made of 4 MB of memory, we have `4MB/PAGE_SIZE` to store.
Thanks to `linker.ld` script we have a symbol in C (`__ld_kernel_end`) defining the last address of memory used by the code. So we can compute the needed size to store the structures and book the space after `__ld_kernel_end` for this.
So from `__ld_kernel_end` to `__ld_kernel_end + MEM_SIZE/PAGE_SIZE` is an array of `struct phyMemDesc` describing all the memory of the computer by chunk of `PAGE_SIZE`


We can setup the pages between `__ld_kernel_begin` and `__ld_kernel_end +MEM_SIZE/PAGE_SIZE` as being used and read the information from the bootloader to setup the free pages.
We will keep one exception for x86 and keep page bellow 0x100000 as used because they are physically mapped to some functions like VGA.

## Allocator

Having lists of used or free physical page make it easy to create a physical page allocator that consist of:
* allocation: Get a `phyMemDesc` from `phyFreePage` and put it in `phyUsedPage`
* free: make sure the page is not used anymore and put it in the `phyFreePage` 
