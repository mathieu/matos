add-symbol-file userspace/user
# Thx to add-gnu-debuglink gdb should know that symbols are in kernel.debug.
# And by default, it should be looking at executable.debug
# But we still have to give him the executable he is suppose to debug (See https://sourceware.org/gdb/current/onlinedocs/gdb.html/Separate-Debug-Files.html)
#add-symbol-file kernel.debug
file kernel
source custom_gdb_extension.py
#For ASM sources
directory arch/x86/:core
target remote | qemu-system-i386 -S  -no-reboot -gdb stdio -kernel kernel -m 16M -serial file:serialOut -hda disk.img
